import os
import re
import glob
import subprocess
from uuid import uuid4

from bitbucket_pipes_toolkit import Pipe, CodeInsights


class AddCustomPatternError(Exception):
    pass


variables = {
    'BITBUCKET_USERNAME': {'type': 'string', 'required': False, 'default': os.getenv('BITBUCKET_REPO_OWNER')},
    'BITBUCKET_REPOSITORY': {'type': 'string', 'required': False, 'default': os.getenv('BITBUCKET_REPO_SLUG')},
    'CUSTOM_PATTERN': {'type': 'string', 'required': False, 'default': ''},
    'FILES': {'type': 'string', 'required': False, 'default': '.'},
    'ANNOTATION_SUMMARY': {'type': 'string', 'required': False, 'default': 'Credentials found in code.'},
    'ANNOTATION_DESCRIPTION': {'type': 'string', 'required': False, 'default': ''},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}

pipe = Pipe(schema=variables)
expected_error_message = '[ERROR] Matched one or more prohibited patterns'


# should match different path format
# ./filename.txt:1
# filename.txt:1
regex = r'(?P<path>\+?[\w].+?[\w]+):(?P<linenumber>\d+)'


def get_or_create_report(commit):
    insights = CodeInsights(repo=pipe.get_variable('BITBUCKET_REPOSITORY'),
                            username=pipe.get_variable('BITBUCKET_USERNAME'))
    expected_external_id = f'git-secrets-scan-report-{commit}'

    reports = insights.get_reports(commit)

    for report in reports['values']:
        if report['external_id'] == expected_external_id:
            return report

    report_data = {
        "type": "report",
        "report_type": "BUG",
        "title": "Git secrets scan",
        "details": "Sensitive data has been discovered in your code by git-secrets-scan.",
        "result": "FAILED",
        "reporter": "Created by git-secrets-scan pipe.",
        "external_id": expected_external_id,
    }
    report = insights.create_report(commit, report_data=report_data)
    return report


def annotate(path, line_number, commit, report_id):

    insights = CodeInsights(repo=pipe.get_variable('BITBUCKET_REPOSITORY'),
                            username=pipe.get_variable('BITBUCKET_USERNAME'))

    annotation_data = {
        "annotation_type": "VULNERABILITY",
        "external_id": str(uuid4()),
        "summary": pipe.get_variable('ANNOTATION_SUMMARY'),
        "details": pipe.get_variable('ANNOTATION_DESCRIPTION'),
        "severity": "HIGH",
        "result": "FAILED",
        "line": line_number,
        "path": path
    }

    annotation = insights.create_annotation(commit, report_id, annotation_data)
    pipe.log_info(annotation)


def main():
    pipe.log_info("Executing the pipe...")

    commit = os.getenv('BITBUCKET_COMMIT')
    files = pipe.get_variable('FILES')
    custom_pattern = pipe.get_variable('CUSTOM_PATTERN')

    if custom_pattern:
        try:
            subprocess.run(['git', 'secrets', '--add', '--global', custom_pattern], text=True, capture_output=True)
        except Exception as e:
            raise AddCustomPatternError(e)

    # https://meta.stackoverflow.com/questions/368287/canonical-for-why-are-wildcards-not-being-expanded-by-subprocess-call-run-popen
    # wildcard preprocessing before run subprocess
    if "*" in files:
        files_list = []
        for file in files.split(' '):
            files_list.extend(glob.glob(file, recursive=True))
    else:
        files_list = files.split(' ')

    # only uniq values
    files_list = list(set(files_list))

    command_list = ['git', 'secrets', '--scan', '--recursive']
    command_list.extend(files_list)

    result = subprocess.run(command_list, text=True, capture_output=True)

    if not result.returncode == 0 and expected_error_message in result.stderr:
        issues = re.findall(regex, result.stderr)
        stderr_to_show = '\n'.join(':'.join(issue) for issue in issues)

        pipe.log_info(f'Found security credentials:\n{stderr_to_show}')

        report = get_or_create_report(commit)

        for path, line_number in issues:
            try:
                annotate(path, line_number, commit, report['uuid'])
            except Exception as error:
                pipe.fail(error)
        pipe.fail('One or more secrets identified in the code!')
    elif not result.returncode == 0:
        raise Exception(result.stderr)
    else:
        pipe.success('No secrets found! You are good at keeping secrets.')

    pipe.success(message="Success!")


if __name__ == '__main__':
    main()
