# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.5.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.4.3

- patch: Fix wildcard usage in FILES variable.

## 0.4.2

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.4.1

- patch: Internal maintenance: Update pipelines.

## 0.4.0

- minor: Add support for CUSTOM_PATTERN variable.

## 0.3.0

- minor: Added new FILES parameter

## 0.2.0

- minor: Add support for authless API requests

## 0.1.0

- minor: Initial release

