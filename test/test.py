import os
import random
from string import ascii_uppercase

from bitbucket_pipes_toolkit.test import PipeTestCase


class GitSecretsScanSuccessTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_success(self):
        result = self.run_container()

        self.assertIn('✔ No secrets found! You are good at keeping secrets.', result)
        self.assertIn('✔ Success!', result)


class GitSecretsScanFailsTestCase(PipeTestCase):
    test_failed_secret_filename = 'test_failed_secrets'
    test_failed_secret_filename_txt = 'test_failed_secrets.txt'
    test_failed_secret_filename_json = 'test_failed_secrets.json'
    test_failed_secret_aws = f"AKIA{''.join(random.choice(ascii_uppercase) for _ in range(16))}"
    test_failed_secret_custom = ''.join([str(x) for x in range(10)]) * 3

    def setUp(self):
        super().setUp()
        with open(self.test_failed_secret_filename, 'w') as f:
            f.write(
                '\n'.join([
                    self.test_failed_secret_aws,
                    self.test_failed_secret_custom
                ])
            )
        with open(self.test_failed_secret_filename_txt, 'w') as f:
            f.write(
                '\n'.join([
                    self.test_failed_secret_aws,
                    self.test_failed_secret_custom
                ])
            )
        with open(self.test_failed_secret_filename_json, 'w') as f:
            f.write(
                '\n'.join([
                    self.test_failed_secret_aws,
                    self.test_failed_secret_custom
                ])
            )

    def tearDown(self):
        super().tearDown()
        os.remove(self.test_failed_secret_filename)
        os.remove(self.test_failed_secret_filename_txt)
        os.remove(self.test_failed_secret_filename_json)

    def test_default_aws(self):
        result = self.run_container()

        self.assertIn('Found security credentials', result)
        self.assertIn(self.test_failed_secret_filename, result)
        self.assertIn(self.test_failed_secret_filename_txt, result)

    def test_custom_pattern(self):
        result = self.run_container(environment={
            "CUSTOM_PATTERN": "^[0-9]{30}$"
        })

        self.assertIn('Found security credentials', result)
        self.assertIn(self.test_failed_secret_filename, result)
        self.assertIn(self.test_failed_secret_filename_txt, result)

    def test_one_file(self):
        result = self.run_container(environment={
            "FILES": self.test_failed_secret_filename
        })

        self.assertIn('Found security credentials', result)
        self.assertIn(self.test_failed_secret_filename, result)

    def test_multiple_files(self):
        result = self.run_container(environment={
            "FILES": f"{self.test_failed_secret_filename} {self.test_failed_secret_filename_txt}"
        })

        self.assertIn('Found security credentials', result)
        self.assertIn(self.test_failed_secret_filename, result)
        self.assertIn(self.test_failed_secret_filename_txt, result)

    def test_wildcard_files(self):
        result = self.run_container(environment={
            "FILES": "*.txt"
        })

        self.assertIn('Found security credentials', result)
        self.assertIn(self.test_failed_secret_filename_txt, result)

    def test_multiple_wildcard_files(self):
        result = self.run_container(environment={
            "FILES": "*.txt *.json"
        })

        self.assertIn('Found security credentials', result)
        self.assertIn(self.test_failed_secret_filename_txt, result)
        self.assertIn(self.test_failed_secret_filename_json, result)
