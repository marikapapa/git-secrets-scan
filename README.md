# Bitbucket Pipelines Pipe: Git secrets scan

Scan your files for hardcoded sensitive data and creates a security report.


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:           
    
```yaml
- pipe: atlassian/git-secrets-scan:0.5.1
  variables:
    # FILES: '<string>' # Optional.
    # CUSTOM_PATTERN: '<string>' # Optional.
    # ANNOTATION_SUMMARY: '<string>' # Optional.
    # ANNOTATION_DESCRIPTION: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                      | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| FILES                         | List of files or directories to scan. Supports glob patterns "*", ".". Default: `.` |
| CUSTOM_PATTERN                | Custom regex pattern that match you custom secrets. Default: `''` (empty string). If default, pipe scans only for AWS credentials. See **Details** Section. |
| ANNOTATION_SUMMARY            | Code annotation summary text. Default: `Credentials found in code.` |
| ANNOTATION_DESCRIPTION        | Detailed description of the annotation. Default: `''` (empty string). |
| DEBUG                         | Turn on extra debug information. Default: `false`. |

_(*) = required variable._


## Details

The pipe by default will scan your files for hardcoded AWS credentials and create a security report with annotations for each found credential.
If `CUSTOM_PATTERN` provided, it will scan for and AWS credentials and `CUSTOM_PATTERN`.
If `CUSTOM_PATTERN` not provided, it will scan for AWS credentials only.


## Examples

### Basic example:

Example running a scan for AWS credentials:
    
```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1

```

Example providing custom annotation summary and description in order to allow teams to set their specific message in the report:
    
```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1
    variables:
      ANNOTATION_SUMMARY: 'My custom security annotation summary'
      ANNOTATION_DESCRIPTION: 'More details about the annotation'

```

Example scanning only `txt` files:
    
```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1
    variables:
      FILES: '*.txt'

```

Example scanning multiple patterns:

```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1
    variables:
      FILES: '*.txt *.json'

```

Example scanning multiple directories:
    
```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1
    variables:
      FILES: 'build test'

```


### Advanced example:

Example scanning you custom secrets that match pattern Bitly's secret key `^R_[0-9a-f]{32}$` file:
```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1
    variables:
      CUSTOM_PATTERN: '^R_[0-9a-f]{32}$'

``` 

Example scanning only `txt` files recursively with `**` pattern. Will match any files and zero or more directories, subdirectories and symbolic links to directories.

Note! Using the `**` pattern in large directory trees may consume an inordinate amount of time.:

```yaml
script:
  - pipe: atlassian/git-secrets-scan:0.5.1
    variables:
      FILES: '**/*.txt'

```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,git,secrets,security
