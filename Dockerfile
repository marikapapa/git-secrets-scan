FROM python:3.7

RUN git clone https://github.com/awslabs/git-secrets.git && \
	cd git-secrets && make install && \
    git secrets --register-aws --global

COPY requirements.txt /
RUN pip install -r requirements.txt
COPY pipe /

ENTRYPOINT ["python3", "/pipe.py"]
